"use strict";

const SKILLS_HASH = "#skills";
const PORTFOLIO_HASH = "#portfolio";
const MENU_SELECTOR = "#responsive-menu"; 
const AJAX_URL = "code/postman.php";
const SUCCESS_COLOR = "#cc9966" ;
const ERROR_COLOR = "#bf1d11";
const SERVER_COLOR = "#333";
let firstVisit = true;
let flag = true;

$(document).ready(function(){
	$('#contactForm').submit(function (e) {
		e.preventDefault();
		let form = $(this);
		let loader = $('#loader');
		let ok = $('#ok');
		function handlerMsg(msg, color){
			ok.text(msg);
			ok.css("color", color);
		}

		loader.css("display","block");

		$.ajax({
			type: "POST",
			url: AJAX_URL,
			data: form.serialize(),
			dataType : "json",
			success: function(data)
			{			
				loader.css("display","none");

				if(data.result === true){
					form.trigger("reset");
					handlerMsg('Your message has been sent successfully!', SUCCESS_COLOR);
				}
				else{
					handlerMsg(data.errors, ERROR_COLOR);
				}
			},
			error: function()
			{
				handlerMsg('Internal server error. Please try again later.', SERVER_COLOR);
			},
			timeout: 5000
		});
	});
	new WOW().init();

	$("a[href^=#]").click(function(e){
	    e.preventDefault();
	    let sc = $(this).attr("href"),
            dn = $(sc).offset().top;

        $('html, body').animate({scrollTop: dn}, 1000);
	});

	$('.row-btn-more a').on("click", function(e){
		flag = !flag;
		if(!flag){
	      $('.row-more-projects').fadeIn( "slow", function() {});
	      $('.row-btn-more a').text("less projects");
		}
		else{
			$('.row-more-projects').fadeOut( "slow", function() {});
	        $('.row-btn-more a').text("more projects");
		}
    });

	$(document).on("scroll", onScroll);

});


function onScroll(){
	let scroll_top = $(document).scrollTop(),
		menu = $('#header').outerHeight();

	$(MENU_SELECTOR + " a").each(function(){
		let hash = $(this).attr("href");
		let target = $(hash);
		if ((target.position().top - menu <= scroll_top)
			&& (target.position().top + target.outerHeight()  > scroll_top)) {
			if((hash === SKILLS_HASH || hash === PORTFOLIO_HASH)
				&& firstVisit){
				fillSkills();
			} else if ((hash !== SKILLS_HASH) && (hash !== PORTFOLIO_HASH)) {
				firstVisit = true;
			}
		}
	});
};

function fillSkills() {
	$('#progressbar1').LineProgressbar({
		percentage: 90,
		fillBackgroundColor: '#da6c0b',
		radius: '10px'
	});
	$('#progressbar2').LineProgressbar({
		percentage: 60,
		fillBackgroundColor: '#cf6100',
		radius: '10px'
	});
	$('#progressbar3').LineProgressbar({
		percentage: 80,
		fillBackgroundColor: '#b45704',
		radius: '10px'
	});
	$('#progressbar4').LineProgressbar({
		percentage: 60,
		fillBackgroundColor: '#974d0c',
		radius: '10px'
	});
	$('#progressbar5').LineProgressbar({
		percentage: 60,
		fillBackgroundColor: '#723804',
		radius: '10px'
	});
	$('#progressbar6').LineProgressbar({
		percentage: 70,
		fillBackgroundColor: '#da6c0b',
		radius: '10px'
	});
	$('#progressbar7').LineProgressbar({
		percentage: 40,
		fillBackgroundColor: '#cf6100',
		radius: '10px'
	});
	$('#progressbar8').LineProgressbar({
		percentage: 40,
		fillBackgroundColor: '#b45704',
		radius: '10px'
	});
	$('#progressbar9').LineProgressbar({
		percentage: 70,
		fillBackgroundColor: '#974d0c',
		radius: '10px'
	});
	$('#progressbar10').LineProgressbar({
		percentage: 60,
		fillBackgroundColor: '#723804',
		radius: '10px'
	});
	firstVisit = false;
}
