<?php
Class PostMan {
    const NAME_KEY = 'name';
    const EMAIL_KEY = 'email';
    const MSG_KEY = 'message';

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderMail;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $errorArr = [];

    /**
     * PostMan constructor.
     * @param string $senderName
     * @param string $senderMail
     * @param string $message
     */
    public function __construct($senderName = "", $senderMail = "", $message = "")
    {
        $this->senderName = $senderName;
        $this->senderMail = $senderMail;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function initiateByPost()
    {
        if (array_key_exists(self::NAME_KEY, $_POST)) {
            $this->senderName = $_POST[self::NAME_KEY];
        } else {
            $this->errorArr[] = sprintf("The '%s' parameter is mandatory", self::NAME_KEY);
        }
        if (array_key_exists(self::EMAIL_KEY, $_POST)) {
            $this->senderMail = $_POST[self::EMAIL_KEY];
        } else {
            $this->errorArr[] = sprintf("The '%s' parameter is mandatory", self::EMAIL_KEY);
        }
        if (array_key_exists(self::MSG_KEY, $_POST)) {
            $this->message = $_POST[self::MSG_KEY];
        } else {
            $this->errorArr[] = sprintf("The '%s' parameter is mandatory", self::MSG_KEY);
        }

        return !count($this->errorArr);
    }

    /**
     * @return bool
     */
    protected function validateParameters()
    {
        $this->senderName = filter_var($this->senderName, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $this->message = filter_var($this->message, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $this->senderMail = filter_var($this->senderMail, FILTER_VALIDATE_EMAIL);
        if (!$this->senderName) {
            $this->errorArr[] = sprintf("The '%s' parameter is invalid", self::NAME_KEY);
        }
        if (!$this->senderMail) {
            $this->errorArr[] = sprintf("The '%s' parameter is invalid", self::EMAIL_KEY);
        }
        if (!$this->message) {
            $this->errorArr[] = sprintf("The '%s' parameter is invalid", self::MSG_KEY);
        }

        return !count($this->errorArr);
    }


    /**
     * @param string $recipientEmail
     * @return bool
     */
    public function send($recipientEmail)
    {
        $result = false;
        if ($this->validateParameters() && $recipientEmail) {

            $result = mail(
                $recipientEmail,
                "From the {$this->senderName} by the mail server",
                "{$this->message} \n{$this->senderName} \n{$this->senderMail}"
            );
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getErrors()
    {
       return implode(", ", $this->errorArr);
    }
}

$result = false;
$errors = '';
$sender = new PostMan();
if ($sender->initiateByPost()) {
    $result = $sender->send("admin@vtsurkanova.top");
}
if (!$result) {
    $errors = $sender->getErrors();
}

echo json_encode(["result" => $result, "errors" => $errors]);
